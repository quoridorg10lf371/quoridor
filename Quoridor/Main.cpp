#include "Player.h"

#include "../Logging/Logging.h"

#include <fstream>

int main()
{
	Board board;
	std::cout << board;

	//UnusedWalls unusedWalls;
	//std::cout << unusedWalls;
	Player player("Joe", Pawn::Color::Red, 3);
	std::cout << player;
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);
	return 0;
}