#include "Player.h"

Player::Player(const std::string& name, const Pawn& pawn , const uint8_t& numberOfWalls) :
	m_name(name),m_pawn(pawn),m_walls(numberOfWalls)
{
}

Player::Player(const Player & other) :
	m_name(other.m_name), m_pawn(other.m_pawn), m_walls(other.m_walls)
{
}

std::string Player::GetName()
{
	return m_name;
}

uint8_t Player::GetNumberOfWalls()
{
	return m_walls;
}

std::ostream & operator<<(std::ostream & outputStream, Player & player)
{
	
	return outputStream << player.m_name << player.m_pawn.GetColor() << player.m_walls;
}

Wall Player::PickWall(std::istream & inputStream) const
{
	return Wall();
}

Board::Position Player::PlaceWall(std::istream & inputStream, Wall && Wall, Board & board)
{
	return Board::Position();
}

Pawn Player::PickPawn(std::string& pawnColor)
{
	Pawn::Color playerColor = FromStringToColor(pawnColor);
	m_pawn.SetColor(playerColor);
	return m_pawn;
}

Board::Position Player::PlacePawn(std::istream & inputStream, Board & board) const
{
	uint16_t line = UINT16_MAX;
	uint16_t column = UINT16_MAX;

	if (inputStream >> line)
		if (inputStream >> column)
		{
			Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column) };

			auto& optionalPiece = board[position];

			if (optionalPiece)
				throw "That position is occupied by another piece.";

			return position;
		}

	inputStream.clear();
	inputStream.seekg(std::ios::end);
	// inputStream.ignore(std::numeric_limits<size_t>::max());

	throw "Please enter only two numbers from 0 to 3.";
}


