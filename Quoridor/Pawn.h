#pragma once

#include <iostream>

class Pawn
{
public:
	enum class Color : uint8_t
	{
		None,
		Black,
		White,
		Blue,
		Red
	};

public:
	Pawn(Color color = Color::None);
	Pawn(const Pawn &other);
	~Pawn();

	Pawn & operator = (const Pawn & other);

	Color GetColor() const;

	void SetColor(const Color color);

	friend std::ostream& operator <<(std::ostream& outputStream, const Pawn& otherPawn);

private:
	Color m_color : 2;
};
