#pragma once

#include "Pawn.h"

class Wall
{
public:
	Wall();
	uint8_t GetWalls();
	~Wall()=default;
private:
	uint8_t m_number;
};

