#pragma once

#include <string>

#include "Board.h"
#include "Wall.h"
#include "Utils.h"

class Player
{
public:
	Player() = default;

	Player(const Player & other);

	Player(const std::string& name, const Pawn& pawn , const uint8_t& numberOfWalls);

	~Player() = default;

	std::string GetName();

	uint8_t GetNumberOfWalls();

	friend std::ostream& operator<<(std::ostream& outputStream, Player& player);

	Wall PickWall(std::istream& inputStream) const;

	Board::Position PlaceWall(std::istream& inputStream, Wall&& wall, Board& board);

	Pawn PickPawn(std::string& pawnColor);

	Board::Position PlacePawn(std::istream& inputStream, Board& board) const;

private:
	std::string m_name;
	Pawn m_pawn;
	uint8_t m_walls;
};

