#include "Board.h"


const char kEmptyBoardCell[] = "____";

const std::optional<Pawn>& Board::operator[](const Position& position) const
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
		throw "Board index out of bound.";

	return m_Pawns[line * kWidth + column];
}

/*std::optional<Pawn>& Board::operator[](const Position& position)
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
		throw "Board index out of bound.";

	return m_Pawns[line * kWidth + column];
}*/

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	bool notWall = false;
	for (auto line = 1; line <= Board::kHeight; line++)
	{
		for (auto column = 1; column <= Board::kWidth; column++)
		{
			if (line % 2 == 0)
				os << 0 << " ";
			else if (column % 2 == 0)
				os << 0 << " ";
			else if (line % 2 != 0 && column % 2 != 0)
				os << 1 << " ";
		}
		os << std::endl;
	}
	

	//DO NOT DELETE what is commented. It is commented just to test the for loop.

	/*for (auto line = 0; line < Board::kHeight; ++line)
	{
		for (auto column = 0; column < Board::kWidth; ++column)
		{
			if (board[position])
				os << *board[position];
			else
				os << kEmptyBoardCell;	
			os << ' ';
		}
		os << std::endl;
	}*/

	return os;

}