#pragma once

#include "Pawn.h"

static Pawn::Color FromStringToColor(std::string color)
{

	if (color == "Black")
		return Pawn::Color::Black;
	else if (color == "White")
		return Pawn::Color::White;
	else if (color == "Blue")
		return Pawn::Color::Blue;
	else if (color == "Red")
		return Pawn::Color::Red;

	return Pawn::Color::None;
}