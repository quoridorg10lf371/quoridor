#pragma once

#include "Pawn.h"

#include <array>
#include <optional>

class Board
{
public:
	using Position = std::pair<uint8_t, uint8_t>;

public:
	Board() = default;

	const std::optional<Pawn>& operator[] (const Position& pos) const;
	// Getter and/or Setter
	//std::optional<Pawn>& operator[] (const Position& pos);

	friend std::ostream& operator << (std::ostream& os, const Board& board);

public:
	static const size_t kWidth = 17;//Changed board size to 17x17 to allow walls placing
	static const size_t kHeight = 17;
	static const size_t kSize = kWidth * kHeight;

private:
	std::array<Pawn, kSize> m_Pawns;
};

