#include "QuoridorGame.h"

void QuoridorGame::Run()
{
	Board board;
	Player player;
	UnusedWalls unusedWalls;
	uint8_t numberOfWalls;
	std::string playerName;
	Pawn pawnColor;
	uint8_t numberOfPlayers;
	std::string color;
	bool ok = false;
	std::cout << "Enter the number of players between 2-4 that will play the game : ";
	std::cin >> numberOfPlayers;
	while (ok == false)
	{
		if (numberOfPlayers > 4 || numberOfPlayers < 2)
		{
			std::cout << "The number you inserted is invalid. Please enter another number : ";
			std::cin >> numberOfPlayers;
		}
		else
			break;

	}
	for (uint8_t index = 1; index <= numberOfPlayers; ++index)
	{
		std::cout << "First player name: ";
		std::cin >> playerName;
		std::cout << "Choose a color for your pawn : Black, White, Blue, Red ";
		std::cin >> color;
		Pawn::Color playerColor = FromStringToColor(color);
		Player player(playerName, playerColor , numberOfWalls);
	}
	Player currentPlayer(player);

    while(true)
    {
        system("cls");

        std::cout << "The board looks like this:\n";
        std::cout << board << std::endl;
        std::cout << "The unused pieces are the following:\n";
        std::cout << unusedWalls << std::endl;

        Pawn pickedPawn;
        Wall pickedWall;

        uint8_t turn = 1;

        while(true)
        {
           
            try
            {
				pickedPawn = std::move(currentPlayer.PickPawn(color));
                break;
            }

            catch (const char* errorMessage)
            {  
                std::cout << errorMessage << std::endl;
            }

            if(turn > 1)
            {
                while(true)
                {
                    uint8_t choice;
                    std::cout << "Move pawn or place wall?";
                    std::cout << "1) Move pawn";
                    std::cout << "2) Place wall";
                    std::cin  >> choice;

                    switch(choice)
                    {
                        case 1:
                        {
                            //user chose to move pawn, implementation MIGHT be missing
                            currentPlayer.PlacePawn(std::cin, board);
                            break;
                        }

                        case 2:
                        {
                            //user chose to place a wall, implementation MIGHT be missing
                            //pickedWall = std::move(currentPlayer.get().PlaceWall(std::cin, pickedWall, board));
                            //PlaceWall(inputStream, currentPlayer.get().pickedWall, board);
                            break;
                        }
                            
                    }

                    // try
                    // {
                    //     pickedWall = std::move(currentPlayer.get().PlaceWall(std::cin, pickedWall, board));
                    //     break;
                    // }

                    // catch (const char* errorMessage)
                    // {  
                    //     std::cout << errorMessage << std::endl;
                    // }
        
                }
            }
        }

        
    }
}

//Pawn::Color QuoridorGame::FromStringToColor(std::string color)
//{
//	return Pawn::Color();
//}
