#include "Pawn.h"

Pawn::Pawn(Color color)
{
	m_color = color;
}

Pawn::Pawn(const Pawn & other)
{
	m_color = other.m_color;
}

Pawn::~Pawn()
{
}

Pawn & Pawn::operator=(const Pawn & other)
{
	m_color = other.m_color;
	return *this;
}

Pawn::Color Pawn::GetColor() const
{
	return Color();
}

void Pawn::SetColor(const Color color)
{
	m_color = color;
}

std::ostream& operator <<(std::ostream& outputStream, const Pawn& otherPawn)
{
	outputStream << otherPawn.GetColor();
	return outputStream;
}
