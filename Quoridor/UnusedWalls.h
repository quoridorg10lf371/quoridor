#pragma once

#include "Wall.h"

#include <unordered_map>

class UnusedWalls
{
public:
	UnusedWalls();

	~UnusedWalls() = default;

	Wall PickWall(const std::string& name);

	friend std::ostream& operator <<(std::ostream& outputStream, const UnusedWalls& unusedWalls);

private:
	std::unordered_map<std::string, Wall> m_pool;
};

