#include "stdafx.h"
#include "CppUnitTest.h"

#include "../Quoridor/Board.h"

#include <cassert>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(ConstructorDefault)
	{
	public:

		TEST_METHOD(CostructorDefault)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			for (i = 0; i < Board::kHeight; ++i)
				for (j = 0; j < Board::kWidth; ++j)
					if (board[position].has_value())
						Assert::Fail();
		}

		TEST_METHOD(SquareBracketsConstOperatorNegativePosition)
		{
			const Board board;
			board[{-1, 0}];
			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{-1, 0}];
			});

		};
	};
}