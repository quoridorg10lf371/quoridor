#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\Quoridor\Player.h"
#include <cassert>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(ConstructorWithThreeParameters)
		{
			std::string name = "player";
			Pawn pawn;
			uint8_t numberOfWalls = 8;
			Player player(name, pawn, numberOfWalls);
			Assert::IsTrue(player.GetName() == name);
			Assert::IsTrue(player.GetNumberOfWalls() == numberOfWalls);
		}

	};
}