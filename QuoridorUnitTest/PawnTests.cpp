#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\Quoridor\Pawn.h"
#include <cassert>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(PawnTests)
	{
	public:

		TEST_METHOD(ConstructorWithOneParameter)
		{
			Pawn Pawn(Pawn::Color::Black);
			Assert::IsTrue(Pawn.GetColor() == Pawn::Color::Black);
		}
		
		TEST_METHOD(EqualOperator)
		{
			Pawn pawn1(Pawn::Color::Black);
			Pawn pawn2(Pawn::Color::Black);
			Assert::IsTrue(pawn1.GetColor() == pawn2.GetColor());
		}
		
	};
}